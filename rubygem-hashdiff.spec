# Generated from hashdiff-0.3.2.gem by gem2rpm -*- rpm-spec -*-
%global gem_name hashdiff

Name: rubygem-%{gem_name}
Version: 0.3.2
Release: 1%{?dist}
Summary: HashDiff is a diff lib to compute the smallest difference between two hashes
Group: Development/Languages
License: MIT
URL: https://github.com/liufengyun/hashdiff
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
BuildRequires: ruby(release)
BuildRequires: rubygems-devel
BuildRequires: ruby >= 1.8.7
BuildRequires: rubygem(rspec)
BuildArch: noarch

%description
HashDiff is a diff lib to compute the smallest difference between two hashes.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}.

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n  %{gem_name}-%{version}

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

%build
# Create the gem as gem install only works on a gem file
gem build %{gem_name}.gemspec

# %%gem_install compiles any C extensions and installs the gem into ./%%gem_dir
# by default, so that we can move it into the buildroot in %%install
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/




# Run the test suite
%check
pushd .%{gem_instdir}
rspec spec
popd

%files
%dir %{gem_instdir}
%exclude %{gem_instdir}/.*
%license %{gem_instdir}/LICENSE
# Exclude this file, since it is not the original one.
%exclude %{gem_instdir}/hashdiff.gemspec
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%{gem_instdir}/Gemfile
%doc %{gem_instdir}/README.md
%{gem_instdir}/Rakefile
%doc %{gem_instdir}/changelog.md
%{gem_instdir}/spec

%changelog
* Mon Feb 06 2017 Vít Ondruch <vondruch@redhat.com> - 0.3.2-1
- Initial package
